import type { FormatEnum } from 'sharp';
import * as sharp from 'sharp';
import * as fs from 'fs';
import * as path from 'path';
import { UPLOAD_DIR } from '../../config';

export const converter = (
  imgPath: string,
  token: string,
  toFormat: keyof FormatEnum = 'webp'
) => {
  const resultDir = `${UPLOAD_DIR}/${token}`;
  if (!fs.existsSync(resultDir)) {
    fs.mkdirSync(resultDir);
  }

  return new Promise((resolve, reject) => {
    sharp(imgPath)
      .toFormat(toFormat)
      .toFile(
        `${resultDir}/${path.parse(imgPath).name}.${toFormat}`,
        (err, info) => {
          if (err) {
            reject(err);
          }
          console.log(`${imgPath} converted to ${info.format}`);
          resolve(info);
        }
      );
  });
};
