import async from 'async';
import { converter } from '../converter';

type QueueStatus = 'waiting' | 'in-process' | 'error' | 'success';

type QueueItem = {
  status: QueueStatus;
  stack: string[];
  result?: unknown;
};

type State = Record<string, QueueItem>;

export class Queue {
  private parallels: number;
  private state: State = {};

  constructor(parallels = 3) {
    this.parallels = parallels;
  }

  push(token: string, items: string[]) {
    this.state[token] = {
      stack: items,
      status: 'waiting',
    };
    this.checkQueue();
  }

  private checkQueue() {
    const waitingItemKeys = Object.keys(this.state).filter(
      key => this.state[key].status === 'waiting'
    );
    const inProcessItemKeys = Object.keys(this.state).filter(
      key => this.state[key].status === 'in-process'
    );

    if (
      inProcessItemKeys.length === this.parallels ||
      !waitingItemKeys.length
    ) {
      return;
    }

    const freeParallels = this.parallels - inProcessItemKeys.length;

    for (let i = 0; i < Math.min(freeParallels, waitingItemKeys.length); i++) {
      const key = waitingItemKeys[i];
      this.state[key].status = 'in-process';
      async
        .eachLimit(this.state[key].stack, 1, (path, next) => {
          converter(path, key)
            .then(() => next())
            .catch(next);
        })
        .then(result => {
          this.state[key] = {
            status: 'success',
            stack: [],
            result,
          };
        })
        .catch(error => {
          this.state[key].status = 'error';
        })
        .finally(() => {
          this.checkQueue();
        });
    }
  }

  getResult(id: string) {
    const item = this.state[id];
    if (item) {
      return { status: item.status, result: item.result };
    }
    return null;
  }
}
