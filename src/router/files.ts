import { Request, Response, Router } from 'express';
import * as mimeTypes from 'mime-types';
import { v4 as uuid } from 'uuid';
import * as multer from 'multer';

import { UPLOAD_DIR } from '../config';
import { Queue } from '../services';

const router = Router();
const upload = multer({ dest: UPLOAD_DIR });
const queue = new Queue();

router.post('/files/upload', upload.array('files'), (req, res) => {
  let files: Express.Multer.File[] = [];

  if (req.files) {
    if (Array.isArray(req.files)) {
      files = req.files;
    } else {
      Object.values(req.files).forEach(items => {
        files = files.concat(items);
      });
    }
  }

  const images = files.filter(file => {
    const mimeType = mimeTypes.lookup(file.originalname);
    return mimeType && mimeType.startsWith('image/');
  });
  const token = uuid();

  queue.push(
    token,
    images.map(img => img.path)
  );

  res.json({ token });
});

router.get('/files/:token', async (req: Request, res: Response) => {
  const result = queue.getResult(req.params.token);
  res.json(result ? result : { status: 'NOTFOUND' });
});

export default router;
