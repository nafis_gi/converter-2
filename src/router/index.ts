import { Router } from 'express';
import filesRouter from './files';

const router = Router();

router.get('/', (req, res) => {
  res.send('Hello, world!');
});

router.get('/healthcheck', (req, res) => {
  res.send('OK');
});

router.use(filesRouter);

export default router;
